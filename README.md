# DuplicatesRemover-java-junit-tdd-maven-project

:star: Star me on GitHub/Gitlab — it helps. Thanks!  

This project is test driven development(TDD) of a simple java application with a function ```RemoveDuplicates()```, which removes all duplicates from an unsorted list of email addresses, while leaving the remaining list in the original order. The function can handle 100,000 email addresses containing 50% randomly placed duplicates, in under 1 second on a typical laptop.  

The application has two other helper functions/methods ```ValidateUserInput()``` and ```GenerateEmailList()```, ```ValidateUserInput()``` helps validate user input and the GenerateEmailList() auto generates a list of email addresses containing 50% randomly placed duplicates.  

The application accepts only one number as command line argument, sanitizes the input, removes duplicates and displays some results/benckmarking info. ONLY ```RemoveDuplicates()``` function was benchmarked as prove.  

The application was written in java using Junit for TDD or unit testing framework and Maven as build system. The unit testing is not comprehensive as it covers only the basic happy paths and does not cover edge cases, negative cases or unhappy paths.  

I decided to make it a full application so it can be runnable, testable and confirmable with ease.

## Steps to execute application

### Method 1 (easy)
1. import project into your favorite IDE e.g. Eclipse  
2. sync maven and run the application or unit test from inside the IDE.

### Method 2 (easy but longer)
1. import project into your favorite IDE e.g. Eclipse.  
2. sync maven and export the application as .jar file from inside the IDE.  
3. from a terminal or CLI, cd into the directory where .jar file was saved in step 2.  
4. type ```java -jar nameOfJarFile.jar <argument>``` e.g. ```java -jar DuplicatesRemover.jar 100000```  
5. press ENTER to run program.  

Voila!

### Author
1. Andrew Godwin

### Internet Presence
Website: https://www.andrew-godwin.com  
Github Profile: https://github.com/pasignature    
Gitlab Profile: https://gitlab.com/pasignature  
Linkedin Profile: https://www.linkedin.com/in/pasignature/  
StackOverflow Profile: https://stackoverflow.com/users/5160455/pasignature

