/**
 * 
 */
package com.demo.project;

import java.util.LinkedHashSet;
import java.util.List;

public class DuplicatesRemover {
	
	/**
	 * Main JVM entry point - accepts only one number as argument
	 * 
	 * @param args - number of e-mails to generate
	 */
	public static void main(String[] args) {
		
		// Instantiating the HelperClass
		HelperClass duplicatesRemover = new HelperClass(args);
		
		// Generating email list with duplicates
		List<String> originalEmailList = duplicatesRemover.GenerateEmailList();
		
		// Benchmarking RemoveDuplicates() function as prove
		long start = System.currentTimeMillis();
		LinkedHashSet<String> newEmailList = duplicatesRemover.RemoveDuplicates(originalEmailList);
		long end = System.currentTimeMillis();
		double totalTime = end-start;
		
		// Printing results
		System.out.println("Printing Email List with 50% randonly placed duplicates...");
		System.out.println(originalEmailList +"\n");
		
		// run program with a smaller number e.g. 10 as argument to confirm results at a glance in IDE
		System.out.println("Printing Email List after removing duplicates...");
		System.out.println(newEmailList +"\n");
		
		System.out.println("Size of original email list: " + originalEmailList.size());
		System.out.println("Size of email list after removing duplicates: " + newEmailList.size());
		System.out.println("Run complete.");
		
		System.out.println("Total Time by RemoveDuplicates() function to remove duplicates: " + String.valueOf(totalTime / 1000.0) + " Seconds");
	}
}
