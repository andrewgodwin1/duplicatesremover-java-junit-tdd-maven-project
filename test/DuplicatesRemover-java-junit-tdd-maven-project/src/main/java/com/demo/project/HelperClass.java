package com.demo.project;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;

public class HelperClass {
	
	private int numberOfEmails;

	/**
	 * Constructor initializes the object of HelperClass and 
	 * prepares the new object for use, accepting args of type string array
	 * as argument which the constructor uses to set member variables
	 * 
	 * @param args - number of e-mails to generate
	 */
	public HelperClass(String[] args) {
		
		this.setNumberOfEmails(ValidateUserInput(args));
	}

	/**
	 * @return the numberOfEmails to be generated
	 */
	public int getNumberOfEmails() {
		return numberOfEmails;
	}

	/**
	 * @param numberOfEmails - the numberOfEmails to set
	 */
	public void setNumberOfEmails(int numberOfEmails) {
		this.numberOfEmails = numberOfEmails;
	}
	
	
	/**
	 * Validates user input, accepting only one number as argument
	 * 
	 * @param args - number of email addresses to generate from user input
	 * @return integer numberOfEmails
	 */
	public int ValidateUserInput(String[] args) {
		
		int numberOfEmails = 0;
		
		 if (args.length != 1) {
			 System.out.println("Accepts only one number as argument, please try again.");
		 }else{

		    //This will loop to prompt the user until the input is correct
		    while (true) {
	
		        try {

		            numberOfEmails = Integer.parseInt(args[0]);
	
		            //If everything went fine, break the loop and move on.
		            break;
	
		        } catch (NumberFormatException e) {
		            //If the method Integer.parseInt throws the exception, catch and print the message.
		            System.out.println("Not a valid number, please try again.");
		            break;
		        }
		    }
		}
		
		return numberOfEmails;
	}
	
	
	/**
	 * Generates a list of email addresses 
	 * containing 50% randomly placed duplicates
	 * 
	 * @return list of randomly placed duplicate email addresses
	 */
	public List<String> GenerateEmailList() {
		
		int totlaEmails = getNumberOfEmails();
		
		// making totalEmails always divisible by 2
		if (totlaEmails % 2 != 0) {
			totlaEmails *= 2;
		}

		List<String> emailList = new ArrayList<String>();

		for(int i = 0; i < totlaEmails/2; i++) {
			
			emailList.add("email" + i + "@domain.com");
			emailList.add("email" + i + "@domain.com");
		}
		
		// shuffling to randomized the list
		Collections.shuffle(emailList);
		
		return emailList;
	}


	/**
	 * Removes duplicates from unsorted list of email addresses
	 * leaving the remaining list in the original order
	 * 
	 * @param emailListWithDuplicates - unsorted list of email addresses
	 * @return list of email addresses in their original order
	 */
	public LinkedHashSet<String> RemoveDuplicates(List<String> EmailListWithDuplicates) {
		
		LinkedHashSet<String> nonDuplicateList = new LinkedHashSet<String>(EmailListWithDuplicates);

		return nonDuplicateList;
	}
	
	 
	
}
