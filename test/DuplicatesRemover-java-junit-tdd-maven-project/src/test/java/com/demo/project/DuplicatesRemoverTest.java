package com.demo.project;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.LinkedHashSet;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class DuplicatesRemoverTest {
	
	private static HelperClass dupRemover;
	private static String[] numberOfEmails = {"100000"};
	
	@BeforeAll
	private static void SetUp() {

		dupRemover = new HelperClass(numberOfEmails);
	}

	@AfterAll
	private static void TearDown() {
		
	}
	
	@Test
	public void canInstantiateHelperClass() {
		
		// Instantiating the HelperClass
		HelperClass helperClassObject = new HelperClass(numberOfEmails);
		assertNotNull(helperClassObject);
	}
	
	@Test
	public void HelperClassConstructorCanInitializeMemberVariables() {
		
		// Instantiating the HelperClass
		HelperClass helperClassObject = new HelperClass(numberOfEmails);
		int userInput = helperClassObject.getNumberOfEmails();
		assertEquals(userInput, Integer.parseInt(numberOfEmails[0]));
	}
	
	@Test
	public void canValidateUserInput() {
		
		int userInput = dupRemover.ValidateUserInput(numberOfEmails);
		assertEquals(userInput, Integer.parseInt(numberOfEmails[0]));
	}
	
	@Test
	public void canGenerateEmailList() {
		
		List<String> emailList = dupRemover.GenerateEmailList();
		assertEquals(emailList.size(), Integer.parseInt(numberOfEmails[0]));
	}
	
	@Test
	public void canRemoveDuplicates() {
		
		List<String> emailList = dupRemover.GenerateEmailList();
		LinkedHashSet<String> newEmailList = dupRemover.RemoveDuplicates(emailList);
		assertEquals(newEmailList.size(), Integer.parseInt(numberOfEmails[0]) /2);
	}
	
}
